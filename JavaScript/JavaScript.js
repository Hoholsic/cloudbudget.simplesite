$(document ).ready(function() {
	$('.header__burger').on('click', function(){
     $('.header__burger').toggleClass('active');
     $('.header__navigation').toggleClass('navigation__active');
  });

   var navOffset = $('.header-menu').offset().top;
   $(window).scroll(function(){
       var scrolled = $(this).scrollTop();
       if (scrolled > navOffset) {
       	$('.main-bg').addClass('nav__fixed');
       }
       else if(scrolled < navOffset) {
       	$('.main-bg').removeClass('nav__fixed');
       }
   });

  $(".lang__default-option").click(function (){
    $(this).parent(".lang").toggleClass('active').find('.lang__list').slideToggle(600);
  });
  $(".lang__list li").click(function (){
    const currentele = $(this).html();
    $(".lang__default-option li").html(currentele);
    $(this).parents(".lang").removeClass("active").find('.lang__list').slideUp(600);
  });

  $(".overview__link").on('click', function() {
    $(this).parent(".overview__text").toggleClass("show-content").find('.overview__hide').slideToggle();

    var replaceText = $(this).parent().hasClass("show-content") ? "Learn Less" : "Learn More";
    $(this).text(replaceText);
  });
  $(".features__link").on('click', function() {
    $(this).parent(".features__exceerpt").toggleClass("show-content").find('.features__hide').slideToggle();

    var replaceText = $(this).parent().hasClass("show-content") ? "Read Less" : "Read More";
    $(this).text(replaceText);
  });
   $('.slider').slick({
      arrows: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      prevArrow: $('.slider__arrow-prew'),
      nextArrow: $('.slider__arrow-next'),
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1
          }
        }
      ]
   });
});
